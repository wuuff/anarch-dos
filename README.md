# Anarch DOS Port

This is an unofficial DOS port of a highly portable Doom-like FPS called [Anarch](https://drummyfish.gitlab.io/anarch).

This port runs in the 320x200, 256-color VGA mode.  It features PC speaker sound effects and mouse support.  However, it doesn't support saving yet.  Most of the keybindings from [here](https://gitlab.com/drummyfish/anarch/-/raw/master/media/manual.png) have been implemented, with some minor exceptions (for example, square brackets switch between weapons, and not all numpad keys are bound).

I have tested this port in FreeDOS on a Pentium 4 machine circa 2004, a Pentium II laptop circa 1998/1999, and a 233MHz Pentium MMX laptop circa 1998, but so far nothing older than that.  I think this should be able to run on a 386.

As far as I can tell, the game may have compatibility issues with USB keyboards.  If you encounter issues, you can try to run the game in "quirk mode" (`anarch.exe q`) to fix the keyboard, but this mode will break mouse support (and can render your mouse unusable until a reboot).

## Building

I compiled this port in [FreeDOS](http://freedos.org/) 1.2 using the [OpenWatcom](http://openwatcom.org/) compiler, running in a VM.  Once you have OpenWatcom set up, just run COMPILE.BAT to build.

I have also partially implemented building with [djgpp](http://www.delorie.com/djgpp/), but it is not functional.  The code for keyboard input will need to be changed.

Both of these compilers are included as packages in FreeDOS, but they require some manual configuration.

## Running

For convenience, the compiled binary is included in the repository.  Simply place *ANARCH.EXE* and *DOS4GW.EXE* together in a folder in DOS and run *ANARCH.EXE*.

## License

The code for this port is licensed under CC0 1.0 Universal, like the original game.  Check LICENSE for details.

DOS32/A is an open-source DOS extender that allows a DOS program to run in 32-bit protected mode.  It's included in this repo as "DOS4GW.EXE" because it is an open-source drop-in replacement for the freeware "DOS/4GW".  Its license is included in DOS32A-LICENSE.  "This product uses DOS/32 Advanced DOS Extender technology."
