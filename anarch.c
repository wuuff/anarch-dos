/**
  @file anarch.c

  Frontend for DOS.  Requires at least a 386, because anarch expects a 32-bit
  system.  Has not been tested on hardware that old.

  The "256-Color VGA Programming in C" tutorial at brackeen.com/vga/bitmaps.html
  was used extensively in making this port.

  DOS frontend by wuuff, 2021

  Released under CC0 1.0 (https://creativecommons.org/publicdomain/zero/1.0/)
  plus a waiver of all other intellectual property. The goal of this work is
  be and remain completely in the public domain forever, available for any use
  whatsoever.
*/

#include <stdint.h>
//#include <unistd.h>
//#include <signal.h>
//#include <fcntl.h>
//#include <linux/input.h>
#include <stdio.h>
//#include <sys/time.h>

//OpenWatcom
#include <i86.h>

#include <time.h>
#include <sys\timeb.h>
#include <conio.h>
#include <dos.h>

//djgpp
//#include <sys/nearptr.h>
//#include <stdlib.h>

//#include "smallinput.h"

//#define SFG_SCREEN_RESOLUTION_X (320/4)
#define SFG_SCREEN_RESOLUTION_X (320)
#define SFG_SCREEN_RESOLUTION_Y (200)
#define SFG_DITHERED_SHADOW 1
#define SFG_FPS 35 

#include "game.h"

#define SCREENSIZE ((SFG_SCREEN_RESOLUTION_X + 1) * SFG_SCREEN_RESOLUTION_Y + 1)

uint32_t timeStart;

uint8_t quirk_mode = 0;

#define KEY_UP 0
#define KEY_DOWN 1
#define KEY_LEFT 2
#define KEY_RIGHT 3
#define KEY_A 4
#define KEY_B 5
#define KEY_C 6
#define KEY_MAP 7
#define KEY_JUMP 8
#define KEY_MENU 9
#define KEY_STRAFE_LEFT 0xa
#define KEY_STRAFE_RIGHT 0xb
#define KEY_NEXT_WEAPON 0xc
#define KEY_PREV_WEAPON 0xd
#define NUM_KEYS 10
uint8_t keys[NUM_KEYS];

void interrupt (*oldInt9)();

const char scantable[] = 
  "\x00\x0a\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08"
  "\x03\x01\x04\x00\x00\x00\x00\x00\x00\x00\x0e\x0d\x05\x05\x0b\x02"
  "\x0c\x00\x00\x00\x05\x06\x07\x00\x00\x00\x06\x00\x00\x00\x00\x00"
  "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x09\x00\x00\x00\x00\x00\x00"
  "\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x03\x00\x04\x00\x00"
  "\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";

/* Used to read scancodes coming from keyboard */
void interrupt newInt9(){
  uint8_t scancode = inp(0x60);
  
  /* If scancode falls within table, and table value is not zero */
  if( (scancode & 0x7f) < sizeof(scantable) && scantable[scancode&0x7f] ){
    /* A key we care about has been pressed or released */
    if( scancode & 0x80 ){
      /* Key was released */
      keys[(uint8_t)scantable[scancode & 0x7f]-1] = 0;
    }else{
      /* Key was pressed */
      keys[(uint8_t)scantable[scancode & 0x7f]-1] = 1;
    }
  }

  if( !quirk_mode ){
    /* Signal EOI to PIC1 */
    outp(0x20,0x20);
  }else{
    _chain_intr(oldInt9);
  }
}

uint32_t getTime()
{
  struct timeb now;
  ftime(&now);
  return now.time * 1000 + now.millitm;
}

uint8_t *VGA;

uint8_t screen[SFG_SCREEN_RESOLUTION_Y][SFG_SCREEN_RESOLUTION_X];

void SFG_setPixel(uint16_t x, uint16_t y, uint8_t colorIndex)
{
  screen[y][x] = colorIndex;
  //uint16_t offset = (y<<8) + (y<<6) + x;
  //VGA[offset] = colorIndex;
}

uint32_t SFG_getTimeMs()
{
  return getTime() - timeStart;
}

void SFG_save(uint8_t data[SFG_SAVE_SIZE])
{
}

uint8_t SFG_load(uint8_t data[SFG_SAVE_SIZE])
{
  return 0;
}

void SFG_sleepMs(uint16_t timeMs)
{
  delay(timeMs);
}

uint8_t mouse_enabled = 0;

void SFG_getMouseOffset(int16_t *x, int16_t *y)
{
  union REGS regs;
  uint16_t a,b;
  if( !mouse_enabled ) return;
  //_disable();
  /* INT 33,B: Read Mouse Motion Counters */
  regs.h.ah = 0x00;
  regs.h.al = 0x0B;
  int386(0x33, &regs, &regs);
  a = regs.h.cl;
  a |= regs.h.ch << 8;
  b = regs.h.dl;
  b |= regs.h.dh << 8;
  *x = a;
  *y = b;
  //_enable();
  
  /*regs.h.ah = 0x00;
  regs.h.al = 0x00;
  int386(0x33, &regs, &regs);
  */
//  int32_t a,b;

//  input_getMousePos(&a,&b);
//  *x = a;
//  *y = b;
//  input_setMousePos(0,0);
}

void SFG_processEvent(uint8_t event, uint8_t data)
{
}


int8_t SFG_keyPressed(uint8_t key)
{
  union REGS regs;
  //newInt9();
  switch(key){
    case SFG_KEY_UP: return keys[KEY_UP];
    case SFG_KEY_DOWN: return keys[KEY_DOWN];
    case SFG_KEY_LEFT: return keys[KEY_LEFT];
    case SFG_KEY_RIGHT: return keys[KEY_RIGHT];
    case SFG_KEY_A: 
      if( keys[KEY_A] ){
        return keys[KEY_A];
      }
      if( !mouse_enabled ) return 0;
      /* INT 33,3: Get Mouse Position and Button Status */
      regs.h.ah = 0x00;
      regs.h.al = 0x03;
      int386(0x33, &regs, &regs);
      /* Left mouse button status is in lowest bit */
      return (regs.h.bl & 0x01);
    case SFG_KEY_B: return keys[KEY_B];
    case SFG_KEY_C: return keys[KEY_C];
    case SFG_KEY_MAP: return keys[KEY_MAP];
    case SFG_KEY_JUMP: return keys[KEY_JUMP];
    case SFG_KEY_MENU: return keys[KEY_MENU];
    case SFG_KEY_STRAFE_LEFT: return keys[KEY_STRAFE_LEFT];
    case SFG_KEY_STRAFE_RIGHT: return keys[KEY_STRAFE_RIGHT];
    case SFG_KEY_NEXT_WEAPON: 
      if( keys[KEY_NEXT_WEAPON] ){
        return keys[KEY_NEXT_WEAPON];
      }
      if( !mouse_enabled ) return 0;
      /* INT 33,3: Get Mouse Position and Button Status */
      regs.h.ah = 0x00;
      regs.h.al = 0x03;
      int386(0x33, &regs, &regs);
      /* Right mouse button status is in second lowest bit */
      return (regs.h.bl & 0x02);
    case SFG_KEY_PREVIOUS_WEAPON: return keys[KEY_PREV_WEAPON];
  }
/*
  switch (key)
  {
    case SFG_KEY_UP:     return input_getKey('w') || input_getKey(SMALLINPUT_ARROW_UP); break;
    case SFG_KEY_RIGHT:  return input_getKey('d') || input_getKey(SMALLINPUT_ARROW_RIGHT); break;
    case SFG_KEY_DOWN:   return input_getKey('s') || input_getKey(SMALLINPUT_ARROW_DOWN); break;
    case SFG_KEY_LEFT:   return input_getKey('a') || input_getKey(SMALLINPUT_ARROW_LEFT); break;
    case SFG_KEY_A:      return input_getKey('j') || input_getKey(SMALLINPUT_RETURN) || input_getKey(SMALLINPUT_MOUSE_L); break;
    case SFG_KEY_B:      return input_getKey('k') || input_getKey(SMALLINPUT_CTRL); break;
    case SFG_KEY_C:      return input_getKey('l'); break;
    case SFG_KEY_MAP:    return input_getKey(SMALLINPUT_TAB); break;
    case SFG_KEY_JUMP:   return input_getKey(' '); break;
    case SFG_KEY_MENU:   return input_getKey(SMALLINPUT_ESCAPE); break;
    case SFG_KEY_NEXT_WEAPON: return input_getKey('2'); break;
    case SFG_KEY_PREVIOUS_WEAPON: return input_getKey('1'); break;
    case SFG_KEY_CYCLE_WEAPON: return input_getKey('f'); break;
    case SFG_KEY_TOGGLE_FREELOOK: return input_getKey(SMALLINPUT_MOUSE_R); break;
    default:             return 0; break;
  }
*/
return 0;
}

void SFG_setMusic(uint8_t value)
{
}

uint8_t sound_timer = 0;

void SFG_playSound(uint8_t soundIndex, uint8_t volume)
{
  switch( soundIndex ){
    /* bullet shot */
    case 0:
      sound(3000);
      sound_timer = 4;
      break;
    /* door opening */
    case 1:
      sound(1000);
      sound_timer = 30;
      break;
    /* explosion */
    case 2:
      sound(2000);
      sound_timer = 15;
      break;
    /* click */
    case 3:
      sound(6000);
      sound_timer = 2;
      break;
    /* plasma shot, teleport */
    case 4:
      sound(4000);
      sound_timer = 5;
      break;
    /* robot sound */
    case 5:
      sound(5000);
      sound_timer = 2;
      break;
  }
}

int running = 1;

void handleSignal(int signal)
{
  puts("\033[?25h"); // show cursor
  running = 0;
}

/* Set all the colors in the VGA palette to the game's
   custom color palette */
void setVGAPalette(){
  outp(0x03c8, 0);
  // Cycle through all palette indices
  for( uint16_t i = 0; i < 256; i++ ){
    /* stackoverflow "how does one convert 16-bit RGB565 to 24-bit RGB888?"
    There are more accurate/faster methods, but since this is only done
    at program start, I'm not concerned about that. */
    uint16_t color = paletteRGB565[i];
    uint8_t red = 255/31 * (color >> 11);
    uint8_t green = 255/63 * ((color << 5) >> 10);
    uint8_t blue = 255/31 * ((color << 11) >> 11);
    /* VGA only accepts 6-bit values per channel, so we need to divide
       the 8-bit RGB888 values by 4 */ 
    outp(0x03c9, red >> 2); 
    outp(0x03c9, green >> 2); 
    outp(0x03c9, blue >> 2); 
  }
}

int main(int argc, char** argv)
{

  /* Needed for djgpp to disable memory protection */
  /*if( __djgpp_nearptr_enable() == 0 ){
    printf("Error: Unable to disable memory protection\n");
    exit(-1);
  }*/

  union REGS regs;

  if( argc == 2 ){
    if( argv[1][0] == 'q' ){
      quirk_mode = 1;
    }
  }

  /* Initialize mouse/check if mouse driver installed */
  /* INT 33,0: Mouse Reset/Get Mouse Installed Flag */
  regs.h.ah = 0x00;
  regs.h.al = 0x00;
  int386(0x33, &regs, &regs);
  if( regs.h.al == 0xFF ){
    mouse_enabled = 1;
  }

  /* Set video mode to mode 0x13: 320x200, 256 colors */
  regs.h.ah = 0x00;
  regs.h.al = 0x13;
  int386(0x10, &regs, &regs);

  /* Set VGA palette to custom palette */
  setVGAPalette();

  /* Set pointer to video memory */
  VGA = (uint8_t*)0xA0000L/* + __djgpp_conventional_base*/;

  /* Register int 9 handler (signal for keyboard data ready) */
  oldInt9 = _dos_getvect(9);
  _dos_setvect(9, &newInt9);

  timeStart = getTime();

//  input_init(SMALLINPUT_MODE_NORMAL,0,0);
  SFG_init();

  while (running)
  {
    if( sound_timer > 0 ){
      sound_timer--;
    }else{
      nosound();
    }
    //input_update();
    // Consume all keyboard input
    /*while( kbhit() ){
      getch();
    }*/
    /*for( uint8_t i = 0; i < NUM_KEYS; i++ ){
      keys[i] = 0;
    }
    while( kbhit() ){
      char ch = getch();
      switch( ch ){
        case 'w':
          keys[KEY_UP] = 1;
          break;
        case 's':
          keys[KEY_DOWN] = 1;
          break;
        case 'a':
          keys[KEY_LEFT] = 1;
          break;
        case 'd':
          keys[KEY_RIGHT] = 1;
          break;
        case 'j':
          keys[KEY_A] = 1;
          break;
        case 'k':
          keys[KEY_B] = 1;
          break;
        case 'l':
          keys[KEY_C] = 1;
          break;
      }
    }*/

    // Update screen in one go 
    for( uint16_t i = 0; i < 16000; i++ ){
      *((uint32_t*)VGA+i) = *((uint32_t*)screen+i);
    }
    // For quarter width resolution
    /*for( uint16_t r = 0; r < 200; r++ ){
      for( uint16_t c = 0; c < 320/4; c++ ){
        uint8_t pix = screen[r][c];
        *((uint32_t*)(VGA+r*320+c*4)) = (uint32_t)pix | (pix<<8) | (pix<<16) | (pix<<24);
      }
    }*/
  
    if (!SFG_mainLoopBody())
      running = 0;

    // Call DOS interrupt to flush buffer but not read input
    //_disable();
    regs.h.ah = 0x0C;
    regs.h.al = 0x00;
    int386(0x21, &regs, &regs);
    //_enable();
  }

  nosound();

  /* Return video mode to text mode 0x03 */
  regs.h.ah = 0x00;
  regs.h.al = 0x03;
  int386(0x10, &regs, &regs);

  /* Restore old handler */
  _dos_setvect(9, oldInt9);
    
  //__djgpp_nearptr_disable();

  //input_end();
}
